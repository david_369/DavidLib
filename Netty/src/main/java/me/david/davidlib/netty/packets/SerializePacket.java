package me.david.davidlib.netty.packets;

import me.david.davidlib.netty.PacketSerializer;

public interface SerializePacket extends Packet<PacketSerializer, PacketSerializer> {

}
